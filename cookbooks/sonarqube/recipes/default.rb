#
# Cookbook:: sonarqube
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

package 'wget' do
   action :install
end

package 'httpd' do
   action :install
end

package 'unzip' do
   action :install
end

package 'java-1.8.0-openjdk' do
   action :install
end

remote_file '/tmp/mysql-community-release-el7-5.noarch.rpm' do
  source 'http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

package 'mysql-community-release-el7-5.noarch' do
  action :install
  source '/tmp/mysql-community-release-el7-5.noarch.rpm'
  provider Chef::Provider::Package::Rpm
end

package 'mysql-server' do
   action :install
end

group "logs" do
  action :manage
  members ['mysql']
end

service 'mysqld' do
  supports :status => true, :restart => true, :reload => true
  action [ :start ]
end

template "/tmp/mysql-init.sql" do
  source 'mysql-init.sql'
  owner 'root'
  group 'root'
  mode '0644'
end

#execute 'settingup root password and creating sonarqube DB' do
#  command 'mysql < /tmp/mysql-init.sql'
#end

template "/etc/my.cnf" do
  source 'my.cnf'
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[mysqld]', :delayed
end

service 'mysqld' do
  supports :status => true, :restart => true, :reload => true
  action [ :enable, :start ]
end

ark 'sonarqube' do
  url 'https://sonarsource.bintray.com/Distribution/sonarqube/sonarqube-6.6.zip'
  home_dir '/opt/sonarqube'
  prefix_root '/opt'
  owner 'root'
  version '6.6'
end

template "/opt/sonarqube/conf/sonar.properties" do
  source 'sonar.properties'
  owner 'root'
  group 'root'
  mode '0644'
  #notifies :stop, :start, 'service[sonar]', :delayed
end

template "/etc/systemd/system/sonar.service" do
  source 'sonar.service'
  owner 'root'
  group 'root'
  mode '0755'
  #notifies :stop, :start, 'service[sonar]', :delayed
end

service 'sonar' do
  supports :start => true, :stop => true
  action [ :enable, :start ]
end

link '/usr/bin/sonar' do
  to '/opt/sonarqube/bin/linux-x86-64/sonar.sh'
end

template "/etc/httpd/conf.d/sonarqube.example.com.conf" do
  source 'sonarqube.example.com.conf'
  owner 'root'
  group 'root'
  mode '0755'
  #notifies :stop, :start, 'service[httpd]', :delayed
end

service 'httpd' do
  supports :status => true, :restart => true, :reload => true
  action [ :enable, :start ]
end



